#!/bin/bash

set -eu

cd /app/code

if [[ ! -f "/app/data/api_secret" ]]; then
    echo "=> Generate initial app api_secret"
    openssl rand -hex 24 > /app/data/api_secret
fi

if [[ ! -f "/app/data/jwt_secret" ]]; then
    echo "=> Generate initial app jwt_secret"
    openssl rand -hex 24 > /app/data/jwt_secret
fi

if [[ ! -f "/app/data/config.js" ]]; then
    echo "=> Copy initial user config.js"
    cp /app/pkg/user.config.js /app/data/config.js
fi

echo "=> Starting MiroTalk SFU"
exec /usr/local/bin/gosu cloudron:cloudron npm start
