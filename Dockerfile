FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=custom.mirotalksfu depName=mirotalksfu versioning=semver
ARG MIROTALKSFU_VERSION=1.7.69

# https://github.com/miroslavpejic85/mirotalksfu/blame/main/package.json#L3
# commit is patched up by renovate post upgrade task
ARG MIROTALKSFU_COMMIT=cf3ae2da0b8698cd073d7e88a7af4c8bae35fdaa

RUN curl -L https://github.com/miroslavpejic85/mirotalksfu/archive/${MIROTALKSFU_COMMIT}.tar.gz | tar -xz --strip-components 1 -f - -C .
# node >= 18 -https://github.com/miroslavpejic85/mirotalksfu/blame/main/package.json#L57
RUN npm i

COPY config.js /app/code/app/src/config.js
COPY start.sh user.config.js /app/pkg/

WORKDIR /app/pkg
CMD [ "/app/pkg/start.sh" ]
