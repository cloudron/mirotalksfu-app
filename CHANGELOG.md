[0.1.0]
* Initial version

[0.2.0]
* Update MiroTalk SFU to 1.4.0

[0.3.0]
* Update MiroTalk SFU to 1.4.1

[0.4.0]
* Update MiroTalk SFU to 1.4.11

[0.5.0]
* Update MiroTalk SFU to 1.4.12

[0.6.0]
* Update MiroTalk SFU to 1.4.13

[1.0.0]
* First stable version of MiroTalk SFU at 1.4.14

[1.0.1]
* Update MiroTalk SFU to 1.4.15

[1.0.2]
* Update MiroTalk SFU to 1.4.16

[1.0.3]
* Update MiroTalk SFU to 1.4.17

[1.0.4]
* Update MiroTalk SFU to 1.4.18

[1.0.5]
* Update MiroTalk SFU to 1.4.19

[1.0.6]
* Update Mirotalk SFU to 1.4.22

[1.0.7]
* Update Mirotalk SFU to 1.4.23

[1.0.8]
* Update MiroTalk SFU to 1.4.25

[1.0.9]
* Update MiroTalk SFU to 1.4.26
* Use up-to-date mediasoup configs - thanks to Miroslav Pejic

[1.1.0]
* Update MiroTalk SFU to 1.4.31

[1.2.0]
* Update MiroTalk SFU to 1.4.32
* Enable optional OpenID integration
* Allow to customize various UI parts

[1.2.1]
* Exit if user config file does not parse
* Be more careful when applying custom branding configs

[1.2.2]
* Update MiroTalk SFU to 1.4.33

[1.2.3]
* Update MiroTalk SFU to 1.4.34

[1.2.4]
* Update MiroTalk SFU to 1.4.35

[1.2.5]
* Update MiroTalk SFU to 1.4.36

[1.2.6]
* Update MiroTalk SFU to 1.4.44

[1.2.7]
* Update MiroTalk SFU to 1.4.45

[1.2.8]
* Update MiroTalk SFU to 1.4.47

[1.2.9]
* Update MiroTalk SFU to 1.4.48

[1.2.10]
* Update MiroTalk SFU to 1.4.49

[1.2.11]
* Update MiroTalk SFU to 1.4.50

[1.2.12]
* Update MiroTalk SFU to 1.4.51

[1.2.13]
* Update MiroTalk SFU to 1.4.72

[1.2.14]
* Update MiroTalk SFU to 1.4.77

[1.2.15]
* Update MiroTalk SFU to 1.4.78

[1.2.16]
* Update MiroTalk SFU to 1.4.82

[1.2.17]
* Update MiroTalk SFU to 1.4.87

[1.2.18]
* Update MiroTalk SFU to 1.4.88

[1.2.19]
* Update MiroTalk SFU to 1.4.89

[1.2.20]
* Update MiroTalk SFU to 1.4.90

[1.2.21]
* Update MiroTalk SFU to 1.4.92

[1.2.22]
* Update MiroTalk SFU to 1.4.93

[1.2.23]
* Update MiroTalk SFU to 1.4.96

[1.2.24]
* Update MiroTalk SFU to 1.4.98

[1.2.25]
* update mirotalk sfu to 1.4.99

[1.3.0]
* Update mirotalk sfu to 1.5.14

[1.3.1]
* Update mirotalk sfu to 1.5.17

[1.3.2]
* Update mirotalk sfu to 1.5.19

[1.3.3]
* Update mirotalk sfu to 1.5.22

[1.3.4]
* Update mirotalk sfu to 1.5.23

[1.3.5]
* Update mirotalk sfu to 1.5.25

[1.3.6]
* Update mirotalk sfu to 1.5.29

[1.3.7]
* Update mirotalk sfu to 1.5.30

[1.3.8]
* Update mirotalk sfu to 1.5.31

[1.3.9]
* Update mirotalk sfu to 1.5.34

[1.3.10]
* Update mirotalk sfu to 1.5.38

[1.3.11]
* Update mirotalk sfu to 1.5.41

[1.3.12]
* Update mirotalk sfu to 1.5.44

[1.3.13]
* Update mirotalk sfu to 1.5.46

[1.3.14]
* Update mirotalk sfu to 1.5.50

[1.3.15]
* Update mirotalk sfu to 1.5.53

[1.3.16]
* Update mirotalk sfu to 1.5.56

[1.3.17]
* Update mirotalk sfu to 1.5.57

[1.3.18]
* Update mirotalk sfu to 1.5.63

[1.3.19]
* Update mirotalk sfu to 1.5.64

[1.3.20]
* Update mirotalk sfu to 1.5.65

[1.3.21]
* Update mirotalk sfu to 1.5.66

[1.3.22]
* Update mirotalk sfu to 1.5.67

[1.3.23]
* Update mirotalk sfu to 1.5.68

[1.3.24]
* Update mirotalk sfu to 1.5.69

[1.3.25]
* Update mirotalk sfu to 1.5.70

[1.3.26]
* Update mirotalk sfu to 1.5.72

[1.3.27]
* Update mirotalk sfu to 1.5.74

[1.3.28]
* Update Mirotalk SFU to 1.5.75

[1.3.29]
* Update Mirotalk SFU to 1.5.76

[1.3.30]
* Update Mirotalk SFU to 1.5.78

[1.3.31]
* Update Mirotalk SFU to 1.5.80

[1.3.32]
* Update Mirotalk SFU to 1.5.82

[1.3.33]
* Update Mirotalk SFU to 1.5.83

[1.3.34]
* Update Mirotalk SFU to 1.5.84

[1.3.35]
* Update Mirotalk SFU to 1.5.85

[1.3.36]
* Update Mirotalk SFU to 1.5.86

[1.3.37]
* Update Mirotalk SFU to 1.5.88

[1.3.38]
* Update Mirotalk SFU to 1.5.93

[1.3.39]
* Update Mirotalk SFU to 1.5.94

[1.3.40]
* Update Mirotalk SFU to 1.5.99

[1.3.41]
* Update Mirotalk SFU to 1.6.10

[1.3.42]
* Update Mirotalk SFU to 1.6.12

[1.3.43]
* Update Mirotalk SFU to 1.6.21

[1.3.44]
* Update Mirotalk SFU to 1.6.22

[1.3.45]
* Update MiroTalk SFU to 1.6.24

[1.3.46]
* Update MiroTalk SFU to 1.6.27

[1.3.47]
* Update mirotalksfu to 1.6.28

[1.3.48]
* Update mirotalksfu to 1.6.30

[1.3.49]
* Update mirotalksfu to 1.6.31

[1.3.50]
* Update mirotalksfu to 1.6.33

[1.3.51]
* Update mirotalksfu to 1.6.35

[1.3.52]
* Update mirotalksfu to 1.6.36

[1.3.53]
* Update mirotalksfu to 1.6.37

[1.3.54]
* Update mirotalksfu to 1.6.38

[1.3.55]
* Update mirotalksfu to 1.6.39

[1.3.56]
* Update mirotalksfu to 1.6.40

[1.3.57]
* Update mirotalksfu to 1.6.41

[1.3.58]
* Update mirotalksfu to 1.6.42

[1.3.59]
* Update mirotalksfu to 1.6.43

[1.3.60]
* Update mirotalksfu to 1.6.44

[1.3.61]
* Update mirotalksfu to 1.6.45

[1.3.62]
* Update mirotalksfu to 1.6.46

[1.3.63]
* Update mirotalksfu to 1.6.47

[1.3.64]
* Update mirotalksfu to 1.6.48

[1.3.65]
* Update mirotalksfu to 1.6.49

[1.3.66]
* Update mirotalksfu to 1.6.50

[1.3.67]
* Update mirotalksfu to 1.6.52

[1.3.68]
* Update mirotalksfu to 1.6.54

[1.3.69]
* Update mirotalksfu to 1.6.60

[1.3.70]
* Update mirotalksfu to 1.6.62

[1.3.71]
* Update mirotalksfu to 1.6.64

[1.3.72]
* Update mirotalksfu to 1.6.65

[1.3.73]
* Update mirotalksfu to 1.6.66

[1.3.74]
* Update mirotalksfu to 1.6.67

[1.3.75]
* Update mirotalksfu to 1.6.69

[1.3.76]
* Update mirotalksfu to 1.6.71

[1.3.77]
* Update mirotalksfu to 1.6.75

[1.3.78]
* Update mirotalksfu to 1.6.79

[1.3.79]
* Update mirotalksfu to 1.6.80

[1.3.80]
* Update mirotalksfu to 1.6.82

[1.3.81]
* Update mirotalksfu to 1.6.84

[1.4.0]
* Update mirotalksfu to 1.6.89
* Update node to 20.18

[1.4.1]
* Update mirotalksfu to 1.6.90

[1.4.2]
* Update mirotalksfu to 1.6.92

[1.4.3]
* Update mirotalksfu to 1.6.96

[1.4.4]
* Update mirotalksfu to 1.6.99

[1.5.0]
* add multiDomain flag

[1.6.0]
* Update mirotalksfu to 1.7.10

[1.6.1]
* Update mirotalksfu to 1.7.11

[1.6.2]
* Update mirotalksfu to 1.7.14

[1.6.3]
* Update mirotalksfu to 1.7.16

[1.6.4]
* Update mirotalksfu to 1.7.17

[1.6.5]
* make stats configurable

[1.6.6]
* Update mirotalksfu to 1.7.18

[1.6.7]
* Update mirotalksfu to 1.7.19

[1.6.8]
* Update mirotalksfu to 1.7.24
* Fix OpenID login when alias domains are used

[1.6.9]
* Update mirotalksfu to 1.7.25

[1.6.10]
* Update mirotalksfu to 1.7.27

[1.6.11]
* Update mirotalksfu to 1.7.28

[1.6.12]
* Update mirotalksfu to 1.7.35

[1.6.13]
* Update mirotalksfu to 1.7.39

[1.6.14]
* Update mirotalksfu to 1.7.43

[1.6.15]
* Update mirotalksfu to 1.7.47

[1.6.16]
* Update mirotalksfu to 1.7.48

[1.6.17]
* Update mirotalksfu to 1.7.49

[1.6.18]
* Update mirotalksfu to 1.7.52

[1.6.19]
* Update mirotalksfu to 1.7.54

[1.6.20]
* Update mirotalksfu to 1.7.57

[1.6.21]
* Update mirotalksfu to 1.7.58

[1.6.22]
* Update mirotalksfu to 1.7.59

[1.6.23]
* Update mirotalksfu to 1.7.60

[1.6.24]
* Update mirotalksfu to 1.7.61

[1.6.25]
* Update mirotalksfu to 1.7.63

[1.6.26]
* Update mirotalksfu to 1.7.64

[1.7.0]
* Update base image to 5.0.0

[1.7.1]
* Update mirotalksfu to 1.7.66

[1.7.2]
* Update mirotalksfu to 1.7.67

[1.7.3]
* Update mirotalksfu to 1.7.69

