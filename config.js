'use strict';

// This file is just a proxy to merge the release config.template.js with a user writable config.json

const config = require('./config.template.js');

let userConfig = {};
try {
    userConfig = require('/app/data/config.js');
} catch (e) {
    console.error('Invalid user config found at /app/data/config.js:', e);
    process.exit(1);
}

function copy(from, to, key) {
    if (from && from[key]) to[key] = from[key];
}

// we only allow specifics to be configured
copy(userConfig.host, config.host, 'protected');
copy(userConfig.host, config.host, 'user_auth');
copy(userConfig.host, config.host, 'users');

copy(userConfig.presenters, config.presenters, 'list');
copy(userConfig.presenters, config.presenters, 'join_first');

copy(userConfig.redirect, config.redirect, 'enabled');
copy(userConfig.redirect, config.redirect, 'url');

copy(userConfig.stats, config.stats, 'enabled');
copy(userConfig.stats, config.stats, 'src');
copy(userConfig.stats, config.stats, 'id');

if (userConfig.ui) {
    copy(userConfig.ui.brand, config.ui.brand, 'app');
    copy(userConfig.ui.brand, config.ui.brand, 'site');
    copy(userConfig.ui.brand, config.ui.brand, 'meta');
    copy(userConfig.ui.brand, config.ui.brand, 'og');
    copy(userConfig.ui.brand, config.ui.brand, 'html');

    copy(userConfig.ui, config.ui, 'buttons');
}

config.api.keySecret = require('fs').readFileSync('/app/data/api_secret', 'utf8').replace('\n', '');
config.jwt.key = require('fs').readFileSync('/app/data/jwt_secret', 'utf8').replace('\n', '');
config.jwt.exp = '1h';

config.mediasoup.webRtcTransport.listenInfos = [{
    protocol: 'udp',
    ip: '0.0.0.0',
    announcedAddress: '127.0.0.1',
    portRange: { min: 40000, max: 40100 }
}, {
    protocol: 'tcp',
    ip: '0.0.0.0',
    announcedAddress: '127.0.0.1',
    portRange: { min: 40000, max: 40100 }
}];

// configure portrange only one setting for TCP and UDP so the configured portranges must match
config.mediasoup.webRtcTransport.listenInfos[0].portRange.min = parseInt(process.env.SFU_TCP);
config.mediasoup.webRtcTransport.listenInfos[0].portRange.max = parseInt(process.env.SFU_TCP) + parseInt(process.env.SFU_TCP_COUNT);

config.mediasoup.webRtcTransport.listenInfos[1].portRange.min = parseInt(process.env.SFU_TCP);
config.mediasoup.webRtcTransport.listenInfos[1].portRange.max = parseInt(process.env.SFU_TCP) + parseInt(process.env.SFU_TCP_COUNT);

config.oidc = {
    enabled: false,
    config: {}
};
if (process.env.CLOUDRON_OIDC_ISSUER) {
    config.oidc.enabled = true;
    config.server.trustProxy = true;

    config.oidc.config = {
        // CLOUDRON_OIDC_PROVIDER_NAME is not supported
        issuerBaseURL: process.env.CLOUDRON_OIDC_ISSUER,
        baseURL: process.env.CLOUDRON_APP_ORIGIN,
        clientID: process.env.CLOUDRON_OIDC_CLIENT_ID,
        clientSecret: process.env.CLOUDRON_OIDC_CLIENT_SECRET,
        authorizationParams: {
            response_type: 'code',
            scope: 'openid profile email',
        },
        secret: 'todochangethissupersecret',
        authRequired: false,
        auth0Logout: false,
        routes: {
            callback: '/auth/callback',
            login: false,
            logout: '/logout',
        }
    };
}

async function fetchPublicIp() {
    const response = await fetch('https://ipv4.api.cloudron.io/api/v1/helper/public_ip');
    const json = await response.json(); // read response body and parse as JSON
    config.mediasoup.webRtcTransport.listenInfos[0].announcedAddress = json.ip;
    config.mediasoup.webRtcTransport.listenInfos[1].announcedAddress = json.ip;

    console.log('Using public IP for announcing:', json.ip)
}

fetchPublicIp();

module.exports = config;
