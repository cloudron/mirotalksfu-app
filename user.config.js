
// All options at https://github.com/miroslavpejic85/mirotalksfu/blob/main/app/src/config.template.js

module.exports = {
    host: {
        /*
            Host Protection (default: false)
            To enhance host security, enable host protection - user auth and provide valid
            usernames and passwords in the users array.
        */
        protected: false,
        user_auth: false,
        users: [
            /*
            {
                username: 'username',
                password: 'password',
            },
            {
                username: 'username2',
                password: 'password2',
            },
            ...
            */
        ]
    },
    presenters: {
        /*
            By default, the presenter is identified as the first participant to join the room, distinguished by their username and UUID.
            Additional layers can be added to specify valid presenters and co-presenters by setting designated usernames.
        */
        list: [],
        join_first: true, // Set to true for traditional behavior, false to prioritize presenters
    },
    stats: {
        /*
            Umami: https://github.com/umami-software/umami
            We use our Self-hosted Umami to track aggregated usage statistics in order to improve our service.
        */
        enabled: false,
        src: 'https://stats.mirotalk.com/script.js',
        id: '41d26670-f275-45bb-af82-3ce91fe57756',
    }
};
